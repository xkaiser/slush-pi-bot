# Slush-Pi-Bot

This project is a template for implementing a robotics solution using the Raspberry Pi on a real time kernel, and a slushboard. This isn't intended as a complete solution, but rather a starting point from which it may help you begin implementing something useful.
The architecture of this project is intended for cartesional plane robotic arms (think CNC Mill machine), but can easily be adapted to more of an RC car (motor positions are irrelevant to you).
Additionally, this includes the setup to run this application cleanly on boot, so no commands from a terminal are necessary.

### How do I get set up? ###

* Checkout the repo

```
git clone https://xkaiser@bitbucket.org/xkaiser/slush-pi-bot.git

```

* Load C/C++ IDE of your choice. Eclipse is suggested.
* Download the following packages from Eclipse Help -> Install New Software option:
	* Programming Languages>C/C++ Development Tools
	* Mobile and Device Development> GCC C/C++ Cross Compiler Support
* Download the Raspberry Pi tool chain, or build it yourself (should be named armv8-rpi3-linux-gnueabihf)
	* For Windows: https://github.com/sysprogs/gnutoolchains/releases/download/raspberry-gcc6.3.0-r4/raspberry-gcc6.3.0-r4.exe should work
	* For Mac: https://s3.amazonaws.com/jaredwolff/xtools-2016-09-01.dmg  should work
	* For Linux: There should be a multilib package for you to download from your distribution's repository. You're looking for armv8-linux-gnueabihf.
	* Install the tool chain
* Import the project, set up the project to use the tool chain as your cross compiler (link to its path, prefix should be armv8-linux-gnueabihf-, mac one is different)


### How does it work? ###

There are two applications as a part of this library, a low level real time application and a high level non-real time communication application.
The purpose for this is that the real time application will poll the data structure in the shared memory for messages without violating deadlines, where as the communication application can wait for an internet connection to receive data without worrying about deadlines.
The communication application opens a port (6000 by default) to listen for either plain text commands, or configurations in JSON format. It is advised to build another application on top of this which exposes a higher level communication channel (such as a node service that exposes a REST API).
Launch both applications with root (required for opening shared memory objects). Send data to the communication app, and the robot should respond.

### Code / Compiling ###

Eclipse project files have been included for convenience. The CommonIncludes folder includes headers that define the communication stucts between the two applications, so that they always are consistent.

### Raspberry Pi Setup ###

See the Scripts folder for setting up a pi to run this software on boot, which ensures it's ready to go as soon as its plugged in and allowed to boot. You should read the scripts to understand what they do, they aren't dense.


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.