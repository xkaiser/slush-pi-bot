#ifndef _COMPONENT_INTERFACE
#define _COMPONENT_INTERFACE

/**
 * @interface ComponentInterface
 * @brief A template class for each component of the robot.
 */
class ComponentInterface
{
public:
	virtual ~ComponentInterface() {}

	/**
	 * @fn step
	 * Re-evaluate the state machine
	 * @param clockTicks The current clock tick in milliseconds.
	 */
	virtual void step(long long int clockTicks) = 0;

	/**
	 * @fn reportStatus
	 * Report information about the component.
	 */
	virtual void reportStatus(void *) = 0;

	/**
	 * @fn emergencyStop
	 * Immediately stop component interactions.
	 */
	virtual void emergencyStop() = 0;
//	virtual void reset() = 0;
};
#endif
