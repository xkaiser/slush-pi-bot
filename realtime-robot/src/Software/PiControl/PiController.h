/*
 * PiController.h
 */

#ifndef SRC_SOFTWARE_PICONTROL_PICONTROLLER_H_
#define SRC_SOFTWARE_PICONTROL_PICONTROLLER_H_

#include <ConfigStruct.h>
#include <SharedMemoryStructs.h>
#include <array>

#include "../../Hardware/StateDevice/Gripper.h"
#include "../../Utilities/ComponentInterface.h"
#include "../../Utilities/Axis.h"

class SharedMemory;

class ZeroReturnController;

class MotorController;

/**
 * @class PiController
 * @brief The controller for the robot, which determines appropriate motion commands.
 *
 * This is the location to implement any coordinated action routines using different
 * devices in the low level controller. So this should pertain to anything that needs to occur
 * on the real time clock. Anything outside of that requirement should be implemented external
 * to this real time project.
 */
class PiController: public ComponentInterface {
private:

	/** Gripper controller */
	Gripper* gc;

	/** Load/pass states between robot and communication app */
	SharedMemory* sm;

	/** Interface that provides motions commands */
	MotorController* mc;

	/** For triggering and checking zero states */
	ZeroReturnController* zc;

	/** Current state */
	CONTROLLER_STATE state;

	/** Next state to process (Default: #C_READY) */
	CONTROLLER_STATE nextState = C_READY;

	/** Pointer to the next function to process */
	void (PiController::*nextStateFunction)();

	/** Passed information either from the initial configuration or communication app */
	ROBOT_IN block;

	/** Current axis target */
	std::array<axis_pos, NUM_AXES> target;

	/**
	 * @fn waitForMotion
	 * @brief Handles motion commands by holding the current states till motors have reached their targets.
	 *
	 * After each motor has reached its target the #nextStateFunction is processed.
	 *
	 * Sets:
	 * 		- #state : #nextState
	 * 		- #nextState : #PC_READY
	 * 		- #nextStateFunction : 0
	 */
	void waitForMotion();

	/**
	 * @fn deactiveGripper
	 * @brief Deactivates the gripper. #GRIP_OFF
	 */
	void deactivateGripper();
public:

	/**
	 * @param[in] sm A reference to #SharedMemory.
	 * @param[in] mc A reference to the #MotorController, for issuing motion commands.
	 * @param[in] gc A reference to the #Gripper, for controlling #GRIPPER_STATE.
	 * @param[in] zc A reference to the #ZeroReturnController.
 *
	 * Sets:
	 * 		- #state : #PC_NEEDS_ZERO
	 * 		- #nextState : #PC_READY
	 * 		- #nextStateFunction : 0
	 * 		- #target : `{0, 0, 0}`
	 */
	PiController(SharedMemory* sm, MotorController* mc, Gripper* gc, ZeroReturnController* zc);

	virtual ~PiController();

	/**
	 * @fn step
	 * @brief Responsible for determining next appropriate #CONTROLLER_STATE.
	 *
	 * If no errors persist, this function handles the state-machine which drives the
	 * actions of the system.
	 * @param[in] clockTicks The current clock tick in milliseconds.
	 */
	void step(long long int clockTicks);

	/**
	 * @fn reportStatus
	 * @brief Outputs #state to #ROBOT_OUT.
	 * @param[in] ptr A reference to the #ROBOT_OUT struct.
	 */
	void reportStatus(void *);

	/**
	 * @fn emergencyStop
	 * @brief Immediate termination action.
	 *
	 * Stops all motor motion through communication with the #MotorController and
	 * 	sets #state to #C_NEEDS_ZERO.
	 */
	void emergencyStop();

	/**
	 * @fn setState
	 * @brief Set the current #CONTROLLER_STATE from outside the PiController.
	 *
	 * @param[in] newState The desired new controller state.
	 */
	void setState(CONTROLLER_STATE newState) {
		state = newState;
	}

	/**
	 * @fn setNextState
	 * @brief Set the next #CONTROLLER_STATE from outside the Controller, to be processed after
	 * 	the current state.
	 *
	 * @param[in] newState The desired next state.
	 */
	void setNextState(CONTROLLER_STATE newState) {
		nextState = newState;
	}

	/**
	 * @fn getState
	 * @brief Get the current #state.
	 * @return The current controller control #state.
	 */
	CONTROLLER_STATE getState() {
		return state;
	}

	/**
	 * @fn reset
	 * @brief Reset the controller.
	 *
	 * Sets:
	 * 		- #state : #C_READY
	 * 		- #nextState : #C_READY
	 * 		- #nextStateFunction : 0
	 */
	void reset();
};

#endif /* SRC_SOFTWARE_PICONTROL_PICONTROLLER_H_ */
