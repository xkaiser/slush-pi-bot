#include "PiController.h"

#include "../ErrorHandler/ErrorHandler.h"
#include "../MotorController/MotorController.h"
#include "../ZeroReturn/ZeroReturnController.h"

PiController::PiController(SharedMemory* sharedMemoryObj, MotorController* motorControlObj, Gripper* gObj,
		ZeroReturnController* zcObj) {
	sm = sharedMemoryObj;
	mc = motorControlObj;
	gc = gObj;
	zc = zcObj;
	state = C_NEEDS_ZERO;
	block = {0};
	target = {0};
	nextStateFunction = 0;
}

PiController::~PiController() {

}

void PiController::reportStatus(void *ptr) {
	ROBOT_OUT* status = (ROBOT_OUT *) ptr;
	status->pc_status.state = this->state;
}

void PiController::step(long long int clockTicks) {
	ERROR_LEVEL currLevel = ErrorHandler::getInstance()->getErrorLevel();
	if (currLevel == EL_STOP) {
		this->state = C_READY;
	}
	else if(currLevel >= EL_STOP_AND_ZERO) {
		this->state = C_NEEDS_ZERO;
	}

	switch (this->state) {
		case C_NEEDS_ZERO:
			if (zc->isZeroed()) {
				state = C_READY;
			}
			break;
		case C_GRIP_ON:
			gc->activate();
			state = C_READY;
			break;
		case C_GRIP_OFF:
			gc->deactivate();
			state = C_READY;
			break;
		case C_READY:
			//We do nothing but wait for a command to be received.
			break;
		case C_ZERO_RETURN:
			zc->start();
			state = C_ZERO_RETURN_WAIT;
			break;
		case C_ZERO_RETURN_WAIT:
			if (zc->state == ZR_IDLE) {
				state = C_READY;
			}
			break;
		case C_WAIT_FOR_MOTION:
			PiController::waitForMotion();
			break;
		case C_ERROR:
		default:
			//Do nothing
			break;
	}
}

void PiController::deactivateGripper() {
	gc->deactivate();
}

void PiController::waitForMotion() {
	if (mc->hasReachedTarget()) {
		state = nextState;
		nextState = C_READY;
		if (nextStateFunction != 0) {
			//Call the provided function
			(this->*nextStateFunction)();
			nextStateFunction = 0;
		}
	}
}

void PiController::emergencyStop() {
	mc->emergencyStop();
	state = C_NEEDS_ZERO;
}

void PiController::reset() {
	state = C_READY;
	nextState = C_READY;
	nextStateFunction = 0;
}
