/*
 * CommandHandler.cpp
 */

#include "CommandHandler.h"

#include <ConfigStruct.h>
#include <algorithm>
#include <array>
#include <iterator>

#include "../../Hardware/StateDevice/Gripper.h"
#include "../../Hardware/Motors/MotorInterface.h"
#include "../../Utilities/Axis.h"
#include "../ErrorHandler/ErrorHandler.h"
#include "../MotorController/MotorController.h"
#include "../PiControl/PiController.h"
#include "../ZeroReturn/ZeroReturnController.h"

CommandHandler::CommandHandler(SharedMemory * sm, PiController* pc, ZeroReturnController* zeroController,
		MotorController* motorController, Gripper* gripper) {
	this->sm = sm;
	this->pc = pc;
	this->zeroController = zeroController;
	this->motorController = motorController;
	this->gripper = gripper;
}

CommandHandler::~CommandHandler() {
}

void CommandHandler::processCommand(ROBOT_IN *block) {
	if (pc->getState() == C_READY && zeroController->getState() == ZR_IDLE && zeroController->isZeroed()) {
		//Commands that require process not started and ready
		std::array<axis_pos, NUM_AXES> target;
		switch (block->commandStruct.command) {
			case COMMAND_AXIS:
				std::array<axis_pos, NUM_AXES> currentTarget;
				for (int i = 0; i < NUM_AXES; i++) {
					target[i] =
							block->commandStruct.axisCommand[i] <= 0 ?
									block->commandStruct.axisCommand[i] : currentTarget[i];
				}
				motorController->setTarget(target);
				pc->setState(C_WAIT_FOR_MOTION);
				break;
		}
	}
	//Commands that don't require motion ready
	switch (block->commandStruct.command) {
		case COMMAND_ZERO_IF_NEEDED:
			if (!zeroController->isZeroed() || pc->getState() == C_NEEDS_ZERO) {
				pc->setState(C_ZERO_RETURN);
			}
			break;
		case COMMAND_ZERO_RETURN:
			if ((pc->getState() == C_READY || pc->getState() == C_NEEDS_ZERO)
					&& !block->config.runtimeFlags.emergencyStop) {
				pc->setState(C_ZERO_RETURN);
				ErrorHandler::getInstance()->reset();
			}
			break;
		case COMMAND_LOAD_CONFIG:
			motorController->emergencyStop();
			motorController->updateConfig(block->config.axes);
			zeroController->clearZero();
			pc->setState(C_READY);
			break;
		case COMMAND_GRIP_ON:
			gripper->activate();
			break;
		case COMMAND_GRIP_OFF:
			gripper->deactivate();
			break;
		case COMMAND_RESET:
			ErrorHandler::getInstance()->reset();
			for (int i = 0; i < NUM_AXES; i++) {
				motorController->softStop(static_cast<AXIS>(i));
			}
			pc->reset();
			zeroController->reset();
			zeroController->isZeroed() ? pc->setState(C_READY) : pc->setState(C_NEEDS_ZERO);
			gripper->deactivate();
			block->config.runtimeFlags.emergencyStop = false;
			break;
		case COMMAND_IDLE:
		default:
			break;
	}
}
