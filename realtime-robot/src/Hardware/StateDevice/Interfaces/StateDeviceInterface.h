/*
 * IOInterface.h
 */

#ifndef HARDWARE_STATEDEVICE_STATEDEVICEINTERFACE_H_
#define HARDWARE_STATEDEVICE_STATEDEVICEINTERFACE_H_

#include "../../../Utilities/ComponentInterface.h"

/**
 * @interface GripperInterface
 * @brief Provides a generic way to interact with gripper logic.
 *
 * All gripper interactions, live or simulated, must inherit from this
 * 	interface to be properly integrated with the robot software.
 */
class StateDeviceInterface : public ComponentInterface {
public:
	StateDeviceInterface() {}
	virtual ~StateDeviceInterface() {}

	/**
	 * @fn activate
	 * @brief Turn on the device, if the state is #VC_OFF.
	 */
	virtual void activate() = 0;

	/**
	 * @fn deactivate
	 * @brief Turn off the device, if the gripper state is #VC_ON.
	 */
	virtual void deactivate() = 0;

	/**
	 * @fn emergencyStop
	 * @brief Immediately turn of the device.
	 */
	virtual void emergencyStop() = 0;
};



#endif /* HARDWARE_STATEDEVICE_STATEDEVICEINTERFACE_H_ */
