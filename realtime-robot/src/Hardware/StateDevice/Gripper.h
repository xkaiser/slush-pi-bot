#ifndef SRC_HARDWARE_STATEDEVICE_GRIPPER_H_
#define SRC_HARDWARE_STATEDEVICE_GRIPPER_H_

#include "Interfaces/StateDeviceInterface.h"

class Gripper : virtual public StateDeviceInterface {
public:
	Gripper(): state(GRIP_OFF) {}
	virtual ~Gripper() {}

	GRIPPER_STATE getGripperState() {
		return state;
	}

protected:
	GRIPPER_STATE state; /**< The current state of the gripper */
};



#endif /* SRC_HARDWARE_GRIPPER_GRIPPER_H_ */
