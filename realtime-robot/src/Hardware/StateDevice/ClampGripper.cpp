#include "../StateDevice/ClampGripper.h"

#include <slushboard.h>

ClampGripper::ClampGripper(SlushBoard *slushboard) {
	board = slushboard;
	board->setIOState(SLUSH_IO_PORTA, SLUSH_IO_PIN0, 0);
	state = GRIP_OFF;
}

ClampGripper::~ClampGripper() {

}

void ClampGripper::activate() {
	switch (state) {
	case GRIP_ON:
	case GRIP_ERROR:
		//Ignore
		break;
	case GRIP_OFF:
		state = GRIP_ON;
		board->setIOState(SLUSH_IO_PORTA, SLUSH_IO_PIN0, 1);
	}
}

void ClampGripper::deactivate() {
	switch (state) {
	case GRIP_OFF:
	case GRIP_ERROR:
		//Ignore
		break;
	case GRIP_ON:
		state = GRIP_OFF;
		board->setIOState(SLUSH_IO_PORTA, SLUSH_IO_PIN0, 0);
	}
}

void ClampGripper::step(long long int clockTicks) {

}

void ClampGripper::reportStatus(void * robotOutPtr) {
	ROBOT_OUT * rout = (ROBOT_OUT *) robotOutPtr;
	rout->gripStatus.isGripOn = (state == GRIP_ON);
}

void ClampGripper::emergencyStop() {
	deactivate();
}
