#ifndef SRC_HARDWARE_GRIPPER_GRIPPER_H_
#define SRC_HARDWARE_GRIPPER_GRIPPER_H_

#include "Gripper.h"
#include "../../Software/ErrorHandler/ErrorHandler.h"

class SlushBoard;

/**
 * @class ClampGripper
 * @brief A physical implementation of #GripperInterface.
 *
 * The responsibility of the gripper is to toggle the clamp between a #GRIP_ON and #GRIP_OFF state
 * If errors persist gripper component will flag errors and call for a stop of all motion.
 */
class ClampGripper : public Gripper  {
private:
	SlushBoard *board;

public:
	/**
	 * @param[in] slushboard A pointer to the slushboard object.
	 *
	 * Sets:
	 * 		- #state : #GRIP_OFF
	 */
	ClampGripper(SlushBoard *slushboard);
	virtual ~ClampGripper();

	void step(long long int clockTicks);

	/**
	 * @fn reportStatus
	 */
	void reportStatus(void * rout);

	void activate();
	void deactivate();
	void emergencyStop();
};

#endif /* SRC_HARDWARE_GRIPPER_GRIPPER_H_ */
