/*
 * SimulatedGripper.h
 */

#ifndef SRC_HARDWARE_GRIPPER_SIMULATEDGRIPPER_H_
#define SRC_HARDWARE_GRIPPER_SIMULATEDGRIPPER_H_

#include "../Gripper.h"

/**
 * @class SimGripper
 * @brief A virtual gripper.
 *
 * Does not establish physical interactions with hardware, but exists
 * 	as a way to test the logic of the robot gripper software.
 */
class SimVacGripper : public Gripper {
public:
	/**
	 * Sets:
	 * 		- #state : #GRIP_OFF
	 */
	SimVacGripper();
	virtual ~SimVacGripper() {}

	/**
	 * @fn step
	 * @brief *** Not Implemented ***
	 * @param[in] clockTicks The current clock tick in milliseconds.
	 */
	void step(long long int clockTicks);

	/**
	 * @fn reportStatus
	 * @brief Check if the simulated gripper is on and report to #ROBOT_OUT
	 * @param[in] rout A pointer to the #ROBOT_OUT.
	 */
	void reportStatus(void *rout);

	/**
	 * @fn activate
	 * @brief Set the simulated state to #GRIP_ON
	 */
	void activate();

	/**
	 * @fn deactivate
	 * @brief Set the simulated state to #GRIP_OFF
	 */
	void deactivate();

	/**
	 * @fn emergencyStop
	 * @brief Immediately change the simulated state to #GRIP_OFF
	 */
	void emergencyStop();

private:
};

#endif /* SRC_HARDWARE_GRIPPER_SIMULATEDGRIPPER_H_ */
