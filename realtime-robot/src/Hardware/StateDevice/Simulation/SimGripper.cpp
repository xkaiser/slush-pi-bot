/*
 * SimGripper.cpp
 */

#include "SimGripper.h"

SimVacGripper::SimVacGripper() {
	this->state = GRIP_OFF;
}

void SimVacGripper::activate() {
	this->state = GRIP_ON;
}

void SimVacGripper::deactivate() {
	this->state = GRIP_OFF;
}

void SimVacGripper::step(long long int clockTicks) {

}

void SimVacGripper::reportStatus(void *robotOutPtr) {
	ROBOT_OUT * rout = (ROBOT_OUT *) robotOutPtr;
	rout->gripStatus.isGripOn = state == GRIP_ON;
}

void SimVacGripper::emergencyStop() {
	this->deactivate();
}
