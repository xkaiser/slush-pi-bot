#!/bin/bash
SERVICE_NAME=robotService
PATH_TO_ROBOT=/home/pi/pick-robot
PATH_TO_COMMS=/home/pi/nonrealtime-comms
ROBOT_PID_PATH_NAME=/tmp/robot-pid
COMMS_PID_PATH_NAME=/tmp/comms-pid
case $1 in
  start)
    echo "Starting $SERVICE_NAME ..."
      if [ ! -f $ROBOT_PID_PATH_NAME ]; then
        $PATH_TO_ROBOT >> /var/log/robot.log 2>&1&
        echo $! > $ROBOT_PID_PATH_NAME
        sleep 1
        $PATH_TO_COMMS >> /var/log/robot.log 2>&1&
        echo $! > $COMMS_PID_PATH_NAME
        echo "$SERVICE_NAME started ..."
      else
        echo "$SERVICE_NAME is already running ..."
      fi
    ;;
    stop)
         if [ -f $ROBOT_PID_PATH_NAME ]; then
            PID=$(cat $ROBOT_PID_PATH_NAME);
            echo “Robot stoping ..."
            kill $PID;
            echo “Robot stopped ..."
            rm $ROBOT_PID_PATH_NAME
        else
            echo "Robot is not running ..."
        fi
        if [ -f $COMMS_PID_PATH_NAME ]; then
            PID=$(cat $COMMS_PID_PATH_NAME);
            echo "comms stoping ..."
            kill $PID;
            echo "comms stopped ..."
            rm $COMMS_PID_PATH_NAME
        else
            echo "comms is not running ..."
        fi
    ;;
esac
