#!/bin/bash

echo "Specify the path to your eclipse-workspace (ie. /Users/your-user-name/ <path/to/eclipse-workspace> ):"

read workspace

echo "Selected workspace: ${workspace}"

eclipse \
	-nosplash \
	-application org.eclipse.cdt.managedbuilder.core.headlessbuild \
	-data ~/${workspace} \
	-cleanBuild "realtime-robot" \
	-cleanBuild "nonrealtime-comms"

