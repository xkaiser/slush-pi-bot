#!/bin/bash

IP_ADDRESS=ip_data.txt

binaries=( \
	../realtime-robot/Debug/realtime-robot \
	../nonrealtime-comms/Debug/nonrealtime-comms \
)

while read line;
do
	if ! [[ $line = '#'* ]] ;
	then
		scp ${binaries[@]} pi@${line}:~/
	fi
done < ${IP_ADDRESS}
