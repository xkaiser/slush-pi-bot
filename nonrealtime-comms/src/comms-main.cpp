#include <asm-generic/socket.h>
#include <ConfigStruct.h>
#include <errno.h>
#include <json.hpp>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <SharedMemoryStructs.h>
#include <unistd.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <arpa/inet.h>
#include "ConfigParser.h"
#include "SharedMemory.h"

#define PORT 6000
#define USEC_PER_SEC		1000000L
#define NSEC_PER_SEC		1000000000L
#define NANO_INC            1000000L
static inline void tsnorm(struct timespec *ts) {
	while (ts->tv_nsec >= NSEC_PER_SEC) {
		ts->tv_nsec -= NSEC_PER_SEC;
		ts->tv_sec++;
	}
}

using namespace std;

using json = nlohmann::json;


json *robotStatusToJSON(json *jsonObj, ROBOT_OUT robotout);
void displayErrors(ROBOT_OUT rout);
void writeToFile(std::ostream &file, std::string values);
std::string getStatusString(CONTROLLER_STATE status);
std::string getErrorFlag(ERROR_STATUS status);
std::string getErrorLevel(ERROR_LEVEL level);
std::string getErrorLevelInfo(ERROR_LEVEL level);
std::string getErrorInfo(ERROR_STATUS status);
bool compareCommands(char * str1, const char *str2);
void *connectionListener(void*);
void parseStringForCommand(char *buffer, int length);
bool nextInt(char** buffer);
void sendDefaultConfig();
void printCommandInformation();
volatile bool sendCommand = false;
volatile COMMAND command = COMMAND_IDLE;
int target[3];
int invalidTarget[] = { 1, 1, 1 };
ConfigParser configParser;
ROBOT_OUT robotout = { 0 };
ROBOT_IN robotin = { 0 };
SharedMemory *sm;
ERROR_LEVEL lastErrorLevel = EL_NO_ERROR;
json robotStatus;

int main() {
	printCommandInformation();
	sm = new SharedMemory();
	sendDefaultConfig();
	int rc;
	//Create thread
	pthread_t thread;
	rc = pthread_create(&thread, NULL, connectionListener, NULL);
	if (rc) {
		perror("Thread failed to be created");
		exit(1);
	}
	struct timespec timespec;
	clock_gettime(CLOCK_MONOTONIC, &timespec);
	timespec.tv_nsec += NANO_INC;
	tsnorm(&timespec);
	ROBOT_OUT oldStatus;
	oldStatus.block_number = -1;

	std::string filename = "Data/positionData.txt";
	std::ofstream file;
	std::ostringstream loggedValues;
	bool shouldWrite = false;

	while (true) {
		static int clockReturn = 0;
		clockReturn = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &timespec, NULL);
		if (clockReturn != 0) {
			//if (clockReturn != EINTR)
			printf("clock_nanosleep failed. errno: %d clockReturn: %d\n", errno, clockReturn);
			break;
		}
		timespec.tv_nsec += NANO_INC;
		tsnorm(&timespec);

		bool newData = sm->readRobotOut(&robotout);
		if (newData) {

			if (oldStatus.block_number > robotout.block_number) {
				//Resend default config, robot must have been restarted
				sendDefaultConfig();
			}

			if (robotout.runtimeFlags.logAxesData && !file.is_open()) {
				file.open(filename, std::ofstream::out | std::ofstream::app);
				loggedValues << "******** NEW SESSION ********\n";
				robotout.runtimeFlags.realtime ? loggedValues << "REALTIME\n" : loggedValues << "SIMULATED\n";
				writeToFile(file, loggedValues.str());
				loggedValues.str(std::string());
			}

			// Check error states
			displayErrors(robotout);
			lastErrorLevel = robotout.operatingErrors.priorityError;

			oldStatus.block_number = robotout.block_number;
			if (robotout.pc_status.state != oldStatus.pc_status.state) {
				if (robotout.runtimeFlags.logAxesData) {
					loggedValues << robotout.axisStatus.axisPosition[0] << ", " <<
							robotout.axisStatus.axisPosition[1] << ", " <<
							robotout.axisStatus.axisPosition[2] << "\n";
				}
				if (!robotout.axisStatus.isBusy) {//Comment out this line to print live feed of position data. Otherwise prints endpoints
					loggedValues << "Currently " << (robotout.axisStatus.isBusy ? "Moving" : "Idle") << "\n";
					shouldWrite = true;

					printf("Currently %s X: %d Y: %d Z: %d\n", robotout.axisStatus.isBusy ? "Moving" : "Idle",
							robotout.axisStatus.axisPosition[0], robotout.axisStatus.axisPosition[1],
							robotout.axisStatus.axisPosition[2]);
				}
			}

			oldStatus = robotout;
		}
		if (sendCommand) {
			robotin.block_number++;
			robotin.commandStruct.command = command;
			memcpy(robotin.commandStruct.axisCommand, target, sizeof(int) * 3);
			sm->writeRobotIn(&robotin);
			printf("Sent command to robot\n");
			sendCommand = false;
		}

		if (shouldWrite) {
			writeToFile(file, loggedValues.str()) ;
			loggedValues.str(std::string());
			shouldWrite = false;
		}
	}
	return 0;
}

json *robotStatusToJSON(json *jsonObj, ROBOT_OUT robotout) {
	*jsonObj = {
			{ "runtimeFlags", {
					{ "realtime", robotout.runtimeFlags.realtime },
					{ "simulated", robotout.runtimeFlags.simulate },
					{ "ignoreErrorFlags", robotout.runtimeFlags.ignoreErrorFlags }
			}},
			{ "controlStatus", {
					{ "state", getStatusString(robotout.pc_status.state).c_str() },
					{ "isZeroed", robotout.pc_status.state != C_NEEDS_ZERO }
			}},
			{ "axisStatus", {
					{ "currentPostion", {
							{ "X", robotout.axisStatus.axisPosition[X] },
							{ "Y", robotout.axisStatus.axisPosition[Y] },
							{ "Z", robotout.axisStatus.axisPosition[Z] }
					}},
					{ "targetPostion", {
							{ "X", robotout.axisStatus.targetPosition[X] },
							{ "Y", robotout.axisStatus.targetPosition[Y] },
							{ "Z", robotout.axisStatus.targetPosition[Z] }
					}}
			}},
			{ "inErrorState", robotout.operatingErrors.numberOfErrors > 0 },
			{ "emergencyStop", robotout.runtimeFlags.emergencyStop }
	};

	if (robotout.operatingErrors.numberOfErrors) {
		json errors;
			for (unsigned int error = 0; error < ES_NUM_OF_FLAGS; error++) {
				ERROR_LEVEL errorLevel = robotout.operatingErrors.errors[error];
				if (errorLevel > EL_NO_ERROR) {
					errors += {
						{ getErrorFlag(static_cast<ERROR_STATUS>(error)).c_str(), {
							{ "description", getErrorInfo(static_cast<ERROR_STATUS>(error)).c_str() },
							{ "severity", getErrorLevel(static_cast<ERROR_LEVEL>(errorLevel)).c_str() }
						}}
					};
				}
			}
		errors += { "numberOfErrors" ,robotout.operatingErrors.numberOfErrors };
		jsonObj->push_back(json::object_t::value_type("operatingErrors", errors));
	}
	return jsonObj;
}

void printCommandInformation() {
	printf("Command Help:\n");
	printf("###,###,###: \t\tCommands the robot to move to the coordinate space given. First is X, then Y, then Z");
	printf("estop:\t\tImmediately stops machine motion and requires a zero return to resume.\n");
	printf("status:\t\tReports the current state of the machine.\n");
	printf("zero:\t\tZero returns the machine.\n");
	printf("zneeded:\tZero returns the machine only if it is needed.\n");
}

void sendDefaultConfig() {
	json fileConfig;
	if (!configParser.loadJSONFromFile("/home/pi/default_config.json", &fileConfig)) {
		printf("/home/pi/default_config.json is missing.\n");
		exit(1);
	}
	configParser.parseConfig(&robotin, &fileConfig);
	robotin.block_number++;
	sm->writeRobotIn(&robotin);
	printf("Configuration sent.\n");
}

std::string getStatusString(CONTROLLER_STATE status) {
	switch (status) {
		case C_GRIP_ON:
			return "C_VAC_ON";
		case C_GRIP_OFF:
			return "C_VAC_OFF";
		case C_ERROR:
			return "C_ERROR";
		case C_READY:
			return "C_READY";
		case C_WAIT_FOR_MOTION:
			return "C_WAIT_FOR_MOTION";
		case C_ZERO_RETURN:
			return "C_ZERO_RETURN";
		case C_ZERO_RETURN_WAIT:
			return "C_ZERO_RETURN_WAIT";
		case C_NEEDS_ZERO:
			return "C_NEEDS_ZERO";
		default:
			return "STATE UNDEFINED";
	}
}

void *connectionListener(void*) {
	int server_fd;
	int new_socket;
	int valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	int bufferLength = 2048;
	char buffer[bufferLength] = { 0 };
	// Creating socket file descriptor
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT);
	if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	if (listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	while (true) {
		if ((new_socket = accept(server_fd, (struct sockaddr *) &address, (socklen_t*) &addrlen)) < 0) {
			perror("accept");
		}
		while (true) {
			valread = read(new_socket, buffer, 1024);
			buffer[1023] = 0;
			if (valread > 0) {
				if (!sendCommand) {
					parseStringForCommand(buffer, valread);
					if (sendCommand) {
						printf("%s: Command sent by client: %d\n", inet_ntoa(address.sin_addr), command);
					}
				}
				if (compareCommands(buffer, "exit")) {
					close(new_socket);
					break;
				}
				if (compareCommands(buffer, "status")) {
					int size = snprintf(buffer, bufferLength, "\n%s\n %s\n", "======== Status ========",
							robotStatusToJSON(&robotStatus, robotout)->dump().c_str());
					send(new_socket, buffer, size, MSG_DONTWAIT);
				}
				if (compareCommands(buffer, "pstatus")) {
					int prettyPrint = 4;
					int size = snprintf(buffer, bufferLength, "\n%s\n %s\n", "======== Status ========",
							robotStatusToJSON(&robotStatus, robotout)->dump(prettyPrint).c_str()); // Pretty printing
					send(new_socket, buffer, size, MSG_DONTWAIT);
				}
			}
		}
	}
	return NULL;
}

bool compareCommands(char * str1, const char *str2) {
	unsigned int length = strlen(str2);
	return length <= strlen(str1) && strncmp(str1, str2, length) == 0;
}

void parseStringForCommand(char *buffer, int length) {
	bool hasComma = false;
	for (int i = 0; i < length; i++) {
		if (buffer[i] == ',') {
			hasComma = true;
			break;
		}
	}
	if (compareCommands(buffer, "json={")) {
		sendCommand = true;
		command = COMMAND_LOAD_CONFIG;
		json json;
		configParser.loadJSONFromString(std::string(buffer).substr(5), &json);
		configParser.parseConfig(&robotin, &json);
	}

	else if (!hasComma) {
		sendCommand = true;
		if (compareCommands(buffer, "estop")) {
			command = COMMAND_EMERGENCY_STOP;
		} else if (compareCommands(buffer, "gcon")) {
			command = COMMAND_GRIP_ON;
		} else if (compareCommands(buffer, "gcoff")) {
			command = COMMAND_GRIP_OFF;
		} else if (compareCommands(buffer, "zero")) {
			command = COMMAND_ZERO_RETURN;
		} else if (compareCommands(buffer, "zneeded")) {
			command = COMMAND_ZERO_IF_NEEDED;
		} else if (compareCommands(buffer, "reset")) {
			command = COMMAND_RESET;
		}  else {
			sendCommand = false;
		}

	} else {
		//Positive values are invalid - valid will be commanded
		memcpy(target, invalidTarget, sizeof(int) * 3);
		command = COMMAND_AXIS;
		target[0] = strtol(buffer, NULL, 10);
		if (nextInt(&buffer)) {
			target[1] = strtol(buffer, NULL, 10);
			if (nextInt(&buffer)) {
				target[2] = strtol(buffer, NULL, 10);
			}
		}
		sendCommand = true;
	}
}

bool nextInt(char** buffer) {
	while (**buffer != 0 && **buffer != ',') {
		(*buffer)++;
	};
	(*buffer)++;
	return *(*buffer - 1) == ',';
}

void writeToFile(std::ostream &file, std::string values) {
	file << values << std::endl;
	file.flush();
}

std::string getErrorFlag(ERROR_STATUS error) {
	switch (error) {
		case ES_NONPERFORMABLE_COMMAND:
			return "EF_NONPERFORMABLE_COMMAND";
		case ES_WRONG_COMMAND:
			return "EF_WRONG_COMMAND";
		case ES_UNDERVOLTAGE_LOCKOUT:
			return "EF_UNDERVOLTAGE_LOCKOUT";
		case ES_THERMAL_WARNING:
			return "EF_THERMAL_WARNING";
		case ES_THERMAL_SHUTDOWN:
			return "EF_THERMAL_SHUTDOWN";
		case ES_OVERCURRENT_DETECTION:
			return "EF_OVERCURRENT_DETECTION";
		case ES_SENSOR_STALL_DETECTED_ON_A:
			return "EF_SENSOR_STALL_DETECTED_ON_A";
		case ES_SENSOR_STALL_DETECTED_ON_B:
			return "EF_SENSOR_STALL_DETECTED_ON_B";
		case ES_AXIS_TARGET_OUT_OF_BOUNDS:
			return "EF_AXIS_TARGET_OUT_OF_BOUNDS";
		default: return "BAD_ERROR";
	}
}

std::string getErrorLevel(ERROR_LEVEL level) {
	switch(level) {
			case EL_INFO:
				return "EL_INFO";
			case EL_STOP:
				return "EL_STOP";
			case EL_STOP_AND_ZERO:
				return "EL_STOP_AND_ZERO";
			case EL_KILL:
				return "EL_KILL";
			default: return "BAD_ERROR_LEVEL";
		}
}

std::string getErrorLevelInfo(ERROR_LEVEL level) {
	switch(level) {
		case EL_INFO:
			return "Informational. (NO ACTION NEEDED).";
		case EL_STOP:
			return "Stop all motion and sensor readings. (RESET NEEDED).";
		case EL_STOP_AND_ZERO:
			return "Stop all motion, sensor readings, and re-zero the machine. (RESET NEEDED).";
		case EL_KILL:
			return "Hardware failure. Stop all motion and sensor readings. (HARD_REBOOT NEEDED).";
		default: return "BAD_ERROR_LEVEL";
	}
}

std::string getErrorInfo(ERROR_STATUS status) {
	switch (status) {
		case ES_NONPERFORMABLE_COMMAND:
			return "Command cannot be performed. Register attempted to write to is busy.";
		case ES_WRONG_COMMAND:
			return "Command does not exist.";
		case ES_UNDERVOLTAGE_LOCKOUT:
			return "Motor supply voltage low. The current voltage used to power the motors has fallen below the preset allowable minimum."
					"No motion commands can be performed.";
		case ES_THERMAL_WARNING:
			return "Internal temperature exceeded preset thermal warning threshold.";
		case ES_THERMAL_SHUTDOWN:
			return "Internal temperature exceeded preset thermal shutdown level. Power bridges have been disabled.";
		case ES_OVERCURRENT_DETECTION:
			return "Power MOSFETs have exceeded a programmed over-current threshold.";
		case ES_SENSOR_STALL_DETECTED_ON_A:
		case ES_SENSOR_STALL_DETECTED_ON_B:
			return "Speed and/or load angle caused motor stall.";
		case ES_AXIS_TARGET_OUT_OF_BOUNDS:
			return "Commanded target is out of bounds.";
		default: return "BAD_ERROR_STATUS";
	}
}

void displayErrors(ROBOT_OUT rout) {
	if (rout.operatingErrors.numberOfErrors > 0 && lastErrorLevel < rout.operatingErrors.priorityError) {
		lastErrorLevel = rout.operatingErrors.priorityError;
		printf("======== Errors ========\n");
		for (unsigned int error = 0; error < ES_NUM_OF_FLAGS; error++) {
			if (rout.operatingErrors.errors[error] > EL_NO_ERROR) {
				printf("Flagged Error: %s\n", getErrorFlag(static_cast<ERROR_STATUS>(error)).c_str());
			}
		}
		printf("Number of errors: %d\n", rout.operatingErrors.numberOfErrors);
	}
}
