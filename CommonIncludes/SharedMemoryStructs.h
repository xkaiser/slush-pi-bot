#ifndef SHAREDMEMORYSTRUCTS_H
#define SHAREDMEMORYSTRUCTS_H

/**
 * @file SharedMemoryStructs.h
 */

#include "ConfigStruct.h"

/**
 * @def SEC_TO_MILL
 * @brief A define value that is used for converting
 */
#define SEC_TO_MILL 1000

/**
 *  Controller Commands
 *  @brief Commands that the robot can process.
 */
enum COMMAND {
	COMMAND_IDLE = 0, 				/**< IDLE: not implemented */
	COMMAND_EMERGENCY_STOP = 1,		/**< Stop all axis motion and turn off gripper, requires re-zero */
	COMMAND_GRIP_ON = 2,				/**< Turn on the gripper */
	COMMAND_GRIP_OFF = 3,			/**< Turn off the gripper */
	COMMAND_ZERO_RETURN = 4,		/**< Zero each axis */
	COMMAND_AXIS = 5,				/**< Move to a location, within the axis limits */
	COMMAND_LOAD_CONFIG = 6,		/**< Load the 'default_config.json' */
	COMMAND_ZERO_IF_NEEDED = 7,	/**< Re-zero the each axis */
	COMMAND_RESET = 8,				/**< Stop all axis motion and turn off gripper and reset error flags; doesn't require re-zero */
};

/**
 * Controller States
 */
enum CONTROLLER_STATE {
	C_GRIP_ON,							/**< The grip has been turned on manually */
	C_GRIP_OFF,						/**< The grip has been turned off manually */
	C_ERROR,							/**< The controller is in a error state */
	C_READY,							/**< The controller is in a ready state (do nothing state) */
	C_WAIT_FOR_MOTION,					/**< Wait for axis motion to complete */
	C_ZERO_RETURN,						/**< Command robot to zero axes */
	C_ZERO_RETURN_WAIT,				/**< Wait for zeroing to complete */
	C_NEEDS_ZERO,						/**< Re-zero robot */
};

/**
 * Runtime Errors
 */
enum ERROR_STATUS {
	ES_NONPERFORMABLE_COMMAND = 0,		/**< Slushengine: Cannot perform operation (may still be valid command) */
	ES_WRONG_COMMAND,					/**< Slushengine: Did not find the desired command (command not found) */
	ES_UNDERVOLTAGE_LOCKOUT,			/**< Slushengine: Motor supply voltage low */
	ES_THERMAL_WARNING,					/**< Slushengine: Internal temperature exceeded preset thermal warning threshold */
	ES_THERMAL_SHUTDOWN,				/**< Slushengine: Internal temperature exceeded preset thermal shutdown level (Power bridges have been disabled) */
	ES_OVERCURRENT_DETECTION,			/**< Slushengine: Power MOSFETs have exceeded a programmed over-current threshold */
	ES_SENSOR_STALL_DETECTED_ON_A,		/**< Slushengine: Speed and/or load angle caused motor stall on A */
	ES_SENSOR_STALL_DETECTED_ON_B,		/**< Slushengine: Speed and/or load angle caused motor stall on B */
	ES_AXIS_TARGET_OUT_OF_BOUNDS,		/**< Desired target is beyond axis limits */
	ES_NUM_OF_FLAGS						/**< The number of possible error flags */
};

/**
 * Board Status Flags
 */
enum BOARD_STATUS {
	BF_HIGH_IMPEDANCE_STATE = 0, 		/**< Slushengine: the HIZ bit has been switched high */
	BF_BUSY,							/**< The motors are in motion */
	BF_EXTERNAL_SWITCH_OPEN,
	BF_SWITCH_TURN_ON_EVENT,
	BF_STEP_CLOCK_MODE,
	BF_NUM_OF_FLAGS						/**< The number of possible status flags */
};

/**
 * Gripper States
 */
enum GRIPPER_STATE {
	GRIP_ON = 0, 	/**< Grip is on */
	GRIP_OFF, 	/**< Grip is off */
	GRIP_ERROR	/**< State not implemented */
};

/**
 * Motor Motion
 */
enum MOTOR_MOTION {
	MM_STOPPED = 0, 		/**< Motor is stopped */
	MM_ACCEL, 				/**< Motor is accelerating */
	MM_DECEL, 				/**< Motor is decelerating */
	MM_CONSTANT_SPEED		/**< Motor is moving at a constant speed */
};

/**
 * Motor Direction
 */
enum DIRECTION {
	DIR_FORWARD = 0, 		/**< Motor is moving in the forward direction */
	DIR_BACKWARD			/**< Motor is moving in the backward direction */
};

/**
 * Error Levels
 */
enum ERROR_LEVEL {
	EL_NO_ERROR = 0, 		/**< No errors */
	EL_INFO, 				/**< Informational (warning) */
	EL_STOP, 				/**< Immediately stop motors */
	EL_STOP_AND_ZERO, 		/**< Immediately stop motors and require re-zero */
	EL_KILL					/**< Unrecoverable error, Immediately stop motors and require system reboot */
};

/**
 * Robot Axes (labeled motors)
 */
enum AXIS {
	X = 0, 			/**< X-axis */
	Y, 				/**< Y-axis */
	Z				/**< Z-axis */
};

/**
 * @typedef Operating Errors
 */
typedef struct {
	ERROR_LEVEL errors[ES_NUM_OF_FLAGS]; 	/**< An array of reported errors */
	ERROR_LEVEL priorityError;				/**< The most critical reported errors */
	int numberOfErrors;						/**< The number of reported errors */
} OPERATING_ERRORS;

/**
 * @typedef Motor information
 */
typedef struct {
	bool board[BF_NUM_OF_FLAGS];	/**< An array of board status flags */
	MOTOR_MOTION motorMotion;		/**< The current motor motion status */
	DIRECTION direction;			/**< The direction of motor motion */
	DIRECTION slushDir;				/**< The direction passed to the Slushengine */
	int motor;						/**< The current motor */
} MOTOR_DEBUG;

/**
 * @typedef Axis Status
 */
typedef struct {
	int targetPosition[3];		/**< The target axis position */
	int axisPosition[3];		/**< The current axis position */
	bool isBusy;				/**< Is the current axis already in motion */
} AXIS_STATUS;

/**
 * @typedef Control Status
 */
typedef struct {
	CONTROLLER_STATE state;			/**< The current controller state */
} CONTROLLER_STATUS;

/**
 * @typedef Gripper Information
 */
typedef struct {
	bool isGripOn;			/**< Is the grip currently on */
} GRIP_STATUS;

/**
 * @typedef Axis Command Structure
 */
typedef struct {
	char command;			/**< The currently passed command */
	int axisCommand[3];		/**< The coordinates to pass to the axes */
} COMMAND_STRUCT;

/**
 * @typedef Robot In
 * @brief Information receive by the robot
 */
typedef struct {
	COMMAND_STRUCT commandStruct;	/**< The command structure */
	JSON_CONFIG config;				/**< The robot configuration */
	long block_number;				/**< The current block number */
} ROBOT_IN;

/**
 * @typedef Robot Out
 * @brief Information reported by the robot
 */
typedef struct {
	RUNTIME_FLAGS runtimeFlags;				/**< Current runtime flags */
	MOTOR_DEBUG mDebug;						/**< Motor debugging */
	OPERATING_ERRORS operatingErrors;		/**< Current operating errors */
	AXIS_STATUS axisStatus;					/**< Current axis status */
	CONTROLLER_STATUS pc_status; 					/**< Current control status */
	GRIP_STATUS gripStatus;					/**< Current gripper control status */
	long block_number;						/**< Current block number */
} ROBOT_OUT;

#endif /* SHAREDMEMORYSTRUCTS_H */
