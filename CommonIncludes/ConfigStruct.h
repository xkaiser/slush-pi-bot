#ifndef CONFIGSTRUCT_H
#define CONFIGSTRUCT_H
#include <stddef.h>
/**
 * @file ConfigStruct.h
 */

/**
 * @typedef Motor Configuration
 * @brief Configuration of the motor that determines motor ID, speeds/accel (max/min), and steps.
 */
typedef struct {
	bool valid;				/**< Is passed motor configuration valid */
	int motorNumber;		/**< Motor ID */
	int accelCurrent;		/**< Passed current used for motor acceleration */
	int decelCurrent;		/**< Passed current used for motor deceleration */
	int holdCurrent;		/**< Passed current used to hold the motor steady */
	int runCurrent;			/**< Passed current used to move the motor */
	int maxStepsPerSec;		/**< Max steps the motor can take per second */
	int stepsPerRev;		/**< Steps necessary for one revolution of motor */
	double mmPerRev;		/**< Millimeters traveled per motor revolution */
	bool invert;			/**< Slushengine: invert the commanded motor direction */
} MOTOR_CONFIG;

/**
 * @typedef Axis Configuration
 * @brief Determines which axis, the motors on the axis, the staging area, and travel limits.
 */
typedef struct {
	bool valid;				/**< Is passed axis configuration valid */
	MOTOR_CONFIG motor[2];	/**< Number of motors on axis */
	char axisLabel;			/**< Label of axis */
	int stagingArea;		/**< Desired staging area for axis */
	int travelLimitmm;		/**< Travel limits of axis */
} AXIS_CONFIG;

/**
 * @typedef Runtime Flags
 * @brief Flags that determine the high level configuration of software
 */
typedef struct {
	bool realtime;			/**< Is the robot running in realtime */
	bool simulate;			/**< Is axis motion simulated */
	bool logAxesData;		/**< Is the robot recording all axes motion */
	bool ignoreErrorFlags;	/**< Is the robot ignoring error messages */
	bool emergencyStop; 	/**< Has an emergency stop been commanded */
} RUNTIME_FLAGS;

/**
 * @typedef JSON Configuration
 */
 typedef struct {
	RUNTIME_FLAGS runtimeFlags;						/**< Desired runtime flags */
	AXIS_CONFIG axes[3];							/**< Desired axes configurations */
	size_t hash;
} JSON_CONFIG;

#endif
